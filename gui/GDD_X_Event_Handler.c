/*****************************************************************************
 *
 * Software License Agreement
 *
 * Copyright � 2012 Microchip Technology Inc.  All rights reserved.
 * Microchip licenses to you the right to use, modify, copy and distribute
 * Software only when embedded on a Microchip microcontroller or digital
 * signal controller, which is integrated into your product or third party
 * product (pursuant to the sublicense terms in the accompanying license
 * agreement).
 *
 * You should refer to the license agreement accompanying this Software
 * for additional information regarding your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY
 * OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,
 * BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA,
 * COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY
 * CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF),
 * OR OTHER SIMILAR COSTS.
 *
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * AUTO-GENERATED CODE:  Graphics Display Desinger X
 *****************************************************************************/

/*****************************************************************************
 * SECTION:  Includes
 *****************************************************************************/
#include <Graphics/Graphics.h>
#include "GDD_Screens.h"
#include "MessageInterface.h"
#include<stdio.h>
XCHAR mytext[] = "List 1\n";
extern BYTE update;

/******************************************************************************************************
 * Function        : GDDDemoGOLMsgCallback
 * Description    : Event handling code is written as separate if statements
 * param1         :
 * param2         :
 * param3         :
 *****************************************************************************************************/

void GDDDemoGOLMsgCallback(WORD objMsg, OBJ_HEADER *pObj, GOL_MSG *pMsg) {
    /**
     *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     AUTO GENERATED CODE. PLEASE DO NOT MODIFY
     *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     */



    // <START_ID_BTN_MSG_PRESSED_(BTN_2)>
    if (objMsg == BTN_MSG_PRESSED && pObj->ID == (BTN_2)) {
        GDDDemoGoToScreen(1);
        // Add code
        REGISTER_MESSAGE(messageBuffer, BTN_2);


    }
    // <END_ID_BTN_MSG_PRESSED_(BTN_2)>


    // <START_ID_BTN_MSG_PRESSED_(BTN_SD_MAIN)>
    if (objMsg == BTN_MSG_PRESSED && pObj->ID == (BTN_SD_MAIN)) {
        GDDDemoGoToScreen(0);
        // Add code
        free(xcharStringPtr);
        REGISTER_MESSAGE(messageBuffer, BTN_SD_MAIN);

    }
    // <END_ID_BTN_MSG_PRESSED_(BTN_SD_MAIN)>


    // <START_ID_BTN_MSG_PRESSED_(BTN_CAM)>
    if (objMsg == BTN_MSG_PRESSED && pObj->ID == (BTN_CAM)) {
        REGISTER_MESSAGE(messageBuffer, BTN_CAM);

        GDDDemoGoToScreen(4);
    }
    // <END_ID_BTN_MSG_PRESSED_(BTN_CAM)>


    // <START_ID_BTN_MSG_PRESSED_(BTN_DELETE_IMG)>
    if (objMsg == BTN_MSG_PRESSED && pObj->ID == (BTN_DELETE_IMG)) {
        // Add code
        /*checks if a file has been selected for deleting. If not you disable the button else you register the message*/
        LISTITEM *itemSel = LbGetSel((LISTBOX *) GOLFindObject(LSB_3), NULL);
        if (itemSel == NULL)
            SetState((BUTTON*) GOLFindObject(BTN_DELETE_IMG), BTN_DRAW | BTN_DISABLED);
        else {
            REGISTER_MESSAGE(messageBuffer, BTN_DELETE_IMG);
            int i = 0;
            while (itemSel->pText[i] != '\n' && i < 12) {
                fileToDelete[i] = itemSel->pText[i];
                i++;
            }
            fileToDelete[i] = '\0';
            GDDDemoGoToScreen(2);
            free(xcharStringPtr);
        }
    }
    // <END_ID_BTN_MSG_PRESSED_(BTN_DELETE_IMG)>


    // <START_ID_BTN_MSG_PRESSED_(BTN_NO_DELETE)>
    if (objMsg == BTN_MSG_PRESSED && pObj->ID == (BTN_NO_DELETE)) {
        REGISTER_MESSAGE(messageBuffer, BTN_NO_DELETE);
        GDDDemoGoToScreen(1);

    }
    // <END_ID_BTN_MSG_PRESSED_(BTN_NO_DELETE)>


    // <START_ID_BTN_MSG_PRESSED_(BTN_CONFIRM_DEL)>
    if (objMsg == BTN_MSG_PRESSED && pObj->ID == (BTN_CONFIRM_DEL)) {
        // Add code
        delete_image();
        REGISTER_MESSAGE(messageBuffer, BTN_CONFIRM_DEL);
        GDDDemoGoToScreen(1);

    }
    // <END_ID_BTN_MSG_PRESSED_(BTN_CONFIRM_DEL)>


    // <START_ID_BTN_MSG_PRESSED_(BTN_OPEN_IMG)>
    if (objMsg == BTN_MSG_PRESSED && pObj->ID == (BTN_OPEN_IMG)) {
        // Add code
        LISTITEM *itemSel = LbGetSel((LISTBOX *) GOLFindObject(LSB_3), NULL);
        if (itemSel == NULL)
            SetState((BUTTON*) GOLFindObject(BTN_OPEN_IMG), BTN_DRAW | BTN_DISABLED);
        else {
            REGISTER_MESSAGE(messageBuffer, BTN_OPEN_IMG);
            int i = 0;
            while (itemSel->pText[i] != '\n' && i < 12) {
                fileToView[i] = itemSel->pText[i];
                i++;
            }
            fileToView[i] = '\0';
            GDDDemoGoToScreen(3);
            free(xcharStringPtr);
        }
    }
    // <END_ID_BTN_MSG_PRESSED_(BTN_OPEN_IMG)>


    // <START_ID_BTN_MSG_PRESSED_(BTN_VIEW_BACK_SD)>
    if (objMsg == BTN_MSG_PRESSED && pObj->ID == (BTN_VIEW_BACK_SD)) {
        // Add code
        if (GetFileType(fileToView) == IMAGE_TYPE_JPG) {
            SetColor(WHITE); // Black in RGB is dark green in YUV
            ClearDevice();
            SSD1926SetRGB(); // Switching shows a little green flicker
        } else if (GetFileType(fileToView) == IMAGE_TYPE_RGB) {
            // Clear screen back to black before changing the hardware SFR byte swap back
            SetColor(BLACK);
            ClearDevice();
            SetReg(REG_SPECIAL_EFFECTS, rgbReg);

            DisplayDisable(); // De-assert chip select to SSD1926
            FSfclose(rgbFile);
        }

        GDDDemoGoToScreen(1);
        REGISTER_MESSAGE(messageBuffer, BTN_VIEW_BACK_SD);
    }
    // <END_ID_BTN_MSG_PRESSED_(BTN_VIEW_BACK_SD)>


    // <START_ID_BTN_MSG_PRESSED_(BTN_BACK_camera_main)>
    if (objMsg == BTN_MSG_PRESSED && pObj->ID == (BTN_BACK_camera_main)) {
        GDDDemoGoToScreen(0);
        REGISTER_MESSAGE(messageBuffer, BTN_BACK_camera_main);
    }
    // <END_ID_BTN_MSG_PRESSED_(BTN_BACK_camera_main)>


    // <START_ID_BTN_MSG_PRESSED_(BTN_INIT_CAM)>
    if (objMsg == BTN_MSG_PRESSED && pObj->ID == (BTN_INIT_CAM)) {
        // Add code
        REGISTER_MESSAGE(messageBuffer, BTN_INIT_CAM);

        // Add code
    }
    // <END_ID_BTN_MSG_PRESSED_(BTN_INIT_CAM)>



	// EVENT_CODE_END End of GDD X Code Generation
	// DO NOT MODIFY/CHANGE THE ABOVE COMMENT

}
