
/*****************************************************************************
* Microchip Graphics Library
* Graphics Display Designer (GDD) Template
*****************************************************************************
 
* Dependencies:    See INCLUDES section below
* Processor:       PIC24F, PIC24H, dsPIC, PIC32
* Compiler:        MPLAB C30 V3.22, MPLAB C32 V1.05b
* Linker:          MPLAB LINK30, MPLAB LINK32
* Company:         Microchip Technology Incorporated
*
* Software License Agreement
*
* Copyright (c) 2010 Microchip Technology Inc.  All rights reserved.
* Microchip licenses to you the right to use, modify, copy and distribute
* Software only when embedded on a Microchip microcontroller or digital
* signal controller, which is integrated into your product or third party
* product (pursuant to the sublicense terms in the accompanying license
* agreement).  
*
* You should refer to the license agreement accompanying this Software
* for additional information regarding your rights and obligations.
*
* SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY
* KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY
* OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
* PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR
* OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,
 
* BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT
* DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
* INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA,
* COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY
* CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF),
* OR OTHER SIMILAR COSTS.
*
* Author               Date        Comment
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*****************************************************************************/


/***************************************************
*INCLUDE FILES
***************************************************/
#include "Graphics/Graphics.h"
#include "GDD_Screens.h"

/***************************************************
* String literals used in the project
***************************************************/
const XCHAR main_BTN_CAMtext[ ] = "Camera";
const XCHAR main_BTN_2text[ ] = "SD Card";
const XCHAR main_STE_16text[ ] = "Camera Present";
const XCHAR main_STE_SD_STATUStext[ ] = "SD Present";

const XCHAR sd_card_LSB_3text[ ] = " ";
const XCHAR sd_card_STE_4text[ ] = "SD CARD";
const XCHAR sd_card_BTN_SD_MAINtext[ ] = "BACK";
const XCHAR sd_card_BTN_OPEN_IMGtext[ ] = "OPEN IMAGE";
const XCHAR sd_card_BTN_DELETE_IMGtext[ ] = "DELETE IMAGE";

const XCHAR confirm_delete_STE_8text[ ] = "Are you sure you want to delete this Image?";
const XCHAR confirm_delete_BTN_CONFIRM_DELtext[ ] = "YES";
const XCHAR confirm_delete_BTN_NO_DELETEtext[ ] = "NO";
const XCHAR confirm_delete_STE_11text[ ] = "CONFIRM DELETE";

const XCHAR view_image_BTN_VIEW_BACK_SDtext[ ] = "BACK";

const XCHAR camera_BTN_INIT_CAMtext[ ] = "CAPTURE IMAGE";
const XCHAR camera_STE_14text[ ] = "Status";
const XCHAR camera_BTN_BACK_camera_maintext[ ] = "BACK";



/***************************************************
* Scheme Declarations
***************************************************/
GOL_SCHEME* defscheme;


/***************************************************
* Function and global Declarations
***************************************************/
void (*CreateFunctionArray[NUM_GDD_SCREENS])();
void (*CreatePrimitivesFunctionArray[NUM_GDD_SCREENS])();
WORD currentGDDDemoScreenIndex;
BYTE update = 0;
static BYTE updateGPL = 0;

/***************************************************
* Function       : GDDDemoCreateFirstScreen
* Parameters     : none
* Return         : none
* Description    : Creates the first screen
***************************************************/
void GDDDemoCreateFirstScreen(void)
{
    currentGDDDemoScreenIndex = 0;
    update = 1;
    (*CreateFunctionArray[currentGDDDemoScreenIndex])();
}

/***************************************************
* Function      : GDDDemoNextScreen
* Parameters    : none
* Return        : none
* Description   : Updates counter to show next screen
***************************************************/
void GDDDemoNextScreen(void)
{
    currentGDDDemoScreenIndex++;
    if(currentGDDDemoScreenIndex >= NUM_GDD_SCREENS)
    {
        currentGDDDemoScreenIndex = 0;
    }
    update = 1;
}

/***************************************************
* Function      : GDDDemoGoToScreen
* Parameters    : int screenIndex
* Return        : none
* Description   : Show the screen referred by the index
***************************************************/
void GDDDemoGoToScreen(int screenIndex)
{
    currentGDDDemoScreenIndex = screenIndex;
    if(currentGDDDemoScreenIndex >= NUM_GDD_SCREENS)
    {
        currentGDDDemoScreenIndex = 0;
    }
    update = 1;
}

/***************************************************
* Function       : GDDDemoGOLDrawCallback
* Parameters     : none
* Return         : none
* Description    : Callback to do the actual drawing of widgets
***************************************************/
void GDDDemoGOLDrawCallback(void)
{
    if(updateGPL)
    {
        (*CreatePrimitivesFunctionArray[currentGDDDemoScreenIndex])();
        updateGPL = 0;
    }

    if(update)
    {
        (*CreateFunctionArray[currentGDDDemoScreenIndex])();
        if(CreatePrimitivesFunctionArray[currentGDDDemoScreenIndex] != NULL)
        {
            updateGPL = 1;
        }
        update = 0;
    }
}

/***************************************************
* Function       : CreateError
* Parameters     : none
* Return         : none
* Description    : Creates a Error screen 
***************************************************/
void CreateError(char* string)
{
    // Blue Screen Error
    SetColor(119);
    ClearDevice();
    SetColor(-1);

    // Flash Error Message
    if(string == NULL)
        {OutTextXY(0, 0, "Runtime Error.");}
    else
        {OutTextXY(0,0, string);}
}

/***************************************************
* Function 	      :    Createmain
* Parameters      :    none
* Return          :    none
* Description     :    Creates GOL widgets used in screen - main
***************************************************/
void Createmain(void)
{
    GOLFree();
    SetColor(RGBConvert(248, 252, 248));
    ClearDevice();


     if(defscheme != NULL) free(defscheme);
        defscheme = GOLCreateScheme();

    defscheme->Color0 = RGBConvert(32, 168, 224);
    defscheme->Color1 = RGBConvert(16, 132, 168);
    defscheme->TextColor0 = RGBConvert(24, 24, 24);
    defscheme->TextColor1 = RGBConvert(248, 252, 248);
    defscheme->EmbossDkColor = RGBConvert(248, 204, 0);
    defscheme->EmbossLtColor = RGBConvert(24, 116, 184);
    defscheme->TextColorDisabled = RGBConvert(128, 128, 128);
    defscheme->ColorDisabled = RGBConvert(208, 224, 240);
    defscheme->CommonBkColor = RGBConvert(208, 236, 240);
    defscheme->pFont = (void*)&Gentium_16;


    BUTTON *pBTN_CAM;
    pBTN_CAM = BtnCreate(  BTN_CAM, //name
                       22, //left
                       43, //top
                       132, //right
                       83, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)main_BTN_CAMtext, //text
                      defscheme //scheme
                    );

    if(pBTN_CAM==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_2;
    pBTN_2 = BtnCreate(  BTN_2, //name
                       174, //left
                       43, //top
                       284, //right
                       83, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)main_BTN_2text, //text
                      defscheme //scheme
                    );

    if(pBTN_2==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    STATICTEXT *pSTE_16;
    pSTE_16 = StCreate(  STE_16, //name
                       22, //left
                       207, //top
                       142, //right
                       233, //bottom
                       ST_DRAW | ST_CENTER_ALIGN, //state
                       (XCHAR*)main_STE_16text, //text
                      defscheme //scheme
                    );

    if(pSTE_16==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    STATICTEXT *pSTE_SD_STATUS;
    pSTE_SD_STATUS = StCreate(  STE_SD_STATUS, //name
                       174, //left
                       207, //top
                       294, //right
                       232, //bottom
                       ST_DRAW | ST_CENTER_ALIGN, //state
                       (XCHAR*)main_STE_SD_STATUStext, //text
                      defscheme //scheme
                    );

    if(pSTE_SD_STATUS==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }


}
/***************************************************
* Function 	      :    Createsd_card
* Parameters      :    none
* Return          :    none
* Description     :    Creates GOL widgets used in screen - sd_card
***************************************************/
void Createsd_card(void)
{
    GOLFree();
    SetColor(RGBConvert(248, 252, 248));
    ClearDevice();


     if(defscheme != NULL) free(defscheme);
        defscheme = GOLCreateScheme();

    defscheme->Color0 = RGBConvert(32, 168, 224);
    defscheme->Color1 = RGBConvert(16, 132, 168);
    defscheme->TextColor0 = RGBConvert(24, 24, 24);
    defscheme->TextColor1 = RGBConvert(248, 252, 248);
    defscheme->EmbossDkColor = RGBConvert(248, 204, 0);
    defscheme->EmbossLtColor = RGBConvert(24, 116, 184);
    defscheme->TextColorDisabled = RGBConvert(128, 128, 128);
    defscheme->ColorDisabled = RGBConvert(208, 224, 240);
    defscheme->CommonBkColor = RGBConvert(208, 236, 240);
    defscheme->pFont = (void*)&Gentium_16;


    LISTBOX *pLSB_3;
    pLSB_3 = LbCreate(  LSB_3, //name
                       7, //left
                       39, //top
                       136, //right
                       234, //bottom
                       LB_DRAW |LB_SINGLE_SEL, //state
                       (XCHAR*)sd_card_LSB_3text, //text
                      defscheme //scheme
                    );

    if(pLSB_3==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    STATICTEXT *pSTE_4;
    pSTE_4 = StCreate(  STE_4, //name
                       8, //left
                       4, //top
                       312, //right
                       39, //bottom
                       ST_DRAW | ST_CENTER_ALIGN, //state
                       (XCHAR*)sd_card_STE_4text, //text
                      defscheme //scheme
                    );

    if(pSTE_4==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_SD_MAIN;
    pBTN_SD_MAIN = BtnCreate(  BTN_SD_MAIN, //name
                       182, //left
                       167, //top
                       292, //right
                       207, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)sd_card_BTN_SD_MAINtext, //text
                      defscheme //scheme
                    );

    if(pBTN_SD_MAIN==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_OPEN_IMG;
    pBTN_OPEN_IMG = BtnCreate(  BTN_OPEN_IMG, //name
                       182, //left
                       51, //top
                       292, //right
                       91, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)sd_card_BTN_OPEN_IMGtext, //text
                      defscheme //scheme
                    );

    if(pBTN_OPEN_IMG==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_DELETE_IMG;
    pBTN_DELETE_IMG = BtnCreate(  BTN_DELETE_IMG, //name
                       182, //left
                       108, //top
                       292, //right
                       148, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)sd_card_BTN_DELETE_IMGtext, //text
                      defscheme //scheme
                    );

    if(pBTN_DELETE_IMG==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }


}
/***************************************************
* Function 	      :    Createconfirm_delete
* Parameters      :    none
* Return          :    none
* Description     :    Creates GOL widgets used in screen - confirm_delete
***************************************************/
void Createconfirm_delete(void)
{
    GOLFree();
    SetColor(RGBConvert(248, 252, 248));
    ClearDevice();


     if(defscheme != NULL) free(defscheme);
        defscheme = GOLCreateScheme();

    defscheme->Color0 = RGBConvert(32, 168, 224);
    defscheme->Color1 = RGBConvert(16, 132, 168);
    defscheme->TextColor0 = RGBConvert(24, 24, 24);
    defscheme->TextColor1 = RGBConvert(248, 252, 248);
    defscheme->EmbossDkColor = RGBConvert(248, 204, 0);
    defscheme->EmbossLtColor = RGBConvert(24, 116, 184);
    defscheme->TextColorDisabled = RGBConvert(128, 128, 128);
    defscheme->ColorDisabled = RGBConvert(208, 224, 240);
    defscheme->CommonBkColor = RGBConvert(208, 236, 240);
    defscheme->pFont = (void*)&Gentium_16;


    STATICTEXT *pSTE_8;
    pSTE_8 = StCreate(  STE_8, //name
                       12, //left
                       68, //top
                       309, //right
                       111, //bottom
                       ST_DRAW | ST_CENTER_ALIGN, //state
                       (XCHAR*)confirm_delete_STE_8text, //text
                      defscheme //scheme
                    );

    if(pSTE_8==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_CONFIRM_DEL;
    pBTN_CONFIRM_DEL = BtnCreate(  BTN_CONFIRM_DEL, //name
                       25, //left
                       138, //top
                       135, //right
                       178, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)confirm_delete_BTN_CONFIRM_DELtext, //text
                      defscheme //scheme
                    );

    if(pBTN_CONFIRM_DEL==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_NO_DELETE;
    pBTN_NO_DELETE = BtnCreate(  BTN_NO_DELETE, //name
                       182, //left
                       138, //top
                       292, //right
                       178, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)confirm_delete_BTN_NO_DELETEtext, //text
                      defscheme //scheme
                    );

    if(pBTN_NO_DELETE==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    STATICTEXT *pSTE_11;
    pSTE_11 = StCreate(  STE_11, //name
                       25, //left
                       4, //top
                       273, //right
                       39, //bottom
                       ST_DRAW | ST_CENTER_ALIGN, //state
                       (XCHAR*)confirm_delete_STE_11text, //text
                      defscheme //scheme
                    );

    if(pSTE_11==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }


}
/***************************************************
* Function 	      :    Createview_image
* Parameters      :    none
* Return          :    none
* Description     :    Creates GOL widgets used in screen - view_image
***************************************************/
void Createview_image(void)
{
    GOLFree();
    SetColor(RGBConvert(248, 252, 248));
    ClearDevice();


     if(defscheme != NULL) free(defscheme);
        defscheme = GOLCreateScheme();

    defscheme->Color0 = RGBConvert(32, 168, 224);
    defscheme->Color1 = RGBConvert(16, 132, 168);
    defscheme->TextColor0 = RGBConvert(24, 24, 24);
    defscheme->TextColor1 = RGBConvert(248, 252, 248);
    defscheme->EmbossDkColor = RGBConvert(248, 204, 0);
    defscheme->EmbossLtColor = RGBConvert(24, 116, 184);
    defscheme->TextColorDisabled = RGBConvert(128, 128, 128);
    defscheme->ColorDisabled = RGBConvert(208, 224, 240);
    defscheme->CommonBkColor = RGBConvert(208, 236, 240);
    defscheme->pFont = (void*)&Gentium_16;


    BUTTON *pBTN_VIEW_BACK_SD;
    pBTN_VIEW_BACK_SD = BtnCreate(  BTN_VIEW_BACK_SD, //name
                       5, //left
                       196, //top
                       115, //right
                       236, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)view_image_BTN_VIEW_BACK_SDtext, //text
                      defscheme //scheme
                    );

    if(pBTN_VIEW_BACK_SD==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }


}
/***************************************************
* Function 	      :    Createcamera
* Parameters      :    none
* Return          :    none
* Description     :    Creates GOL widgets used in screen - camera
***************************************************/
void Createcamera(void)
{
    GOLFree();
    SetColor(RGBConvert(248, 252, 248));
    ClearDevice();


     if(defscheme != NULL) free(defscheme);
        defscheme = GOLCreateScheme();

    defscheme->Color0 = RGBConvert(32, 168, 224);
    defscheme->Color1 = RGBConvert(16, 132, 168);
    defscheme->TextColor0 = RGBConvert(24, 24, 24);
    defscheme->TextColor1 = RGBConvert(248, 252, 248);
    defscheme->EmbossDkColor = RGBConvert(248, 204, 0);
    defscheme->EmbossLtColor = RGBConvert(24, 116, 184);
    defscheme->TextColorDisabled = RGBConvert(128, 128, 128);
    defscheme->ColorDisabled = RGBConvert(208, 224, 240);
    defscheme->CommonBkColor = RGBConvert(208, 236, 240);
    defscheme->pFont = (void*)&Gentium_16;


    BUTTON *pBTN_INIT_CAM;
    pBTN_INIT_CAM = BtnCreate(  BTN_INIT_CAM, //name
                       6, //left
                       23, //top
                       128, //right
                       63, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)camera_BTN_INIT_CAMtext, //text
                      defscheme //scheme
                    );

    if(pBTN_INIT_CAM==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    STATICTEXT *pSTE_14;
    pSTE_14 = StCreate(  STE_14, //name
                       6, //left
                       82, //top
                       317, //right
                       145, //bottom
                       ST_DRAW, //state
                       (XCHAR*)camera_STE_14text, //text
                      defscheme //scheme
                    );

    if(pSTE_14==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    BUTTON *pBTN_BACK_camera_main;
    pBTN_BACK_camera_main = BtnCreate(  BTN_BACK_camera_main, //name
                       207, //left
                       23, //top
                       317, //right
                       63, //bottom
                       0, //radius
                       BTN_DRAW, //state
                       NULL, //bitmap
                       (XCHAR*)camera_BTN_BACK_camera_maintext, //text
                      defscheme //scheme
                    );

    if(pBTN_BACK_camera_main==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }

    PROGRESSBAR *pPRB_FETCH;
    pPRB_FETCH = PbCreate(  PRB_FETCH, //name
                       6, //left
                       164, //top
                       317, //right
                       224, //bottom
                       PB_DRAW | PB_DRAW_BAR, //state
                       0, //pos
                       100, //range
                      defscheme //scheme
                    );

    if(pPRB_FETCH==NULL)
    {
      CreateError(0);
      while(1); //Fatal Error, Check for memory leak or heap size
    }


}

/***************************************************
* Function       : CreateFunctionArray
* Parameters     : none
* Return         : none
* Description    : Creates a array of GOL function pointers
***************************************************/
void (*CreateFunctionArray[NUM_GDD_SCREENS])(void)=
    
{
    &Createmain,
    &Createsd_card,
    &Createconfirm_delete,
    &Createview_image,
    &Createcamera,
};



/***************************************************
* Function       : CreatePrimitivesFunctionArray
* Parameters     : none
* Return         : none
* Description    : Creates a array of GPL function pointers
***************************************************/
void (*CreatePrimitivesFunctionArray[NUM_GDD_SCREENS])(void)=
    
{
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
};


