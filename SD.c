
#include "SD.h"
#include "MainDemo.h"
#include "MessageInterface.h"
#include<string.h>

/**TODO
 * delete image
 * save image
 * filter file type to browse - done
 * check sd memory size before saving image
 */

BYTE rgbReg;
FSFILE *rgbFile;
const char alphanum[] =
        "MZNXBCVASDFGHJKLPOIUYTREWQ";
BYTE SDStatus = SD_ABSENT;

char SDPresent[] = "SD Card Present";
char SDAbsent[] = "Check SD Card!!";
BYTE initSD=1;

void sd_init() {

   if(SDDetect()==TRUE){
       while(!FSInit());
       initSD=FALSE;
       return;
   }
   initSD=TRUE;
   /* if (MDD_MediaDetect() == TRUE) {
        int ret = FSInit();
        if (ret == TRUE) {
            SDStatus = SD_PRESENT;
            return;
        }
    }
    SDStatus = SD_ABSENT;
    */

}

char *setSDStatus() {

    switch (SDStatus) {
        case SD_PRESENT:
            return SDPresent;
            break;
        case SD_ABSENT:
            return SDAbsent;
            break;
        default:
            break;
    }
}

IMAGE_TYPE GetFileType(char *fileName) {

    while (*fileName != '.')
        fileName++;

    fileName++;

    // check to see if it is a jpeg
    if ((fileName[0] == 'J') && (fileName[1] == 'P') && (fileName[2] == 'G'))
        return IMAGE_TYPE_JPG;

    if ((fileName[0] == 'j') && (fileName[1] == 'p') && (fileName[2] == 'g'))
        return IMAGE_TYPE_JPG;

    // check to see if it is a rgb movie
    if ((fileName[0] == 'R') && (fileName[1] == 'G') && (fileName[2] == 'B'))
        return IMAGE_TYPE_RGB;

    if ((fileName[0] == 'r') && (fileName[1] == 'g') && (fileName[2] == 'b'))
        return IMAGE_TYPE_RGB;
    if ((fileName[0] == 'c') && (fileName[1] == 'm') && (fileName[2] == 'u'))
        return IMAGE_TYPE_CMU;
    if ((fileName[0] == 'C') && (fileName[1] == 'M') && (fileName[2] == 'U'))
        return IMAGE_TYPE_CMU;

    return IMAGE_TYPE_OTHER;
}

void sd_open_image(char *name) {
    /*TODO add suppport for other types*/

    SearchRec records;

    if (-1 != FindFirst("*.*", ATTR_ARCHIVE | ATTR_READ_ONLY | ATTR_HIDDEN, &records)) {
        do {
            if (!strcmp(fileToView, records.filename)) {
                if (GetFileType(records.filename) == IMAGE_TYPE_JPG) {
                    SetColor(BLACK); // Black in RGB is dark green in YUV
                    ClearDevice();
                    SSD1926SetYUV(); // Switching shows a little green flicker
                    JPEGPutImage(records.filename);
                    //DelayMs(2000);

                }
                if (GetFileType(records.filename) == IMAGE_TYPE_RGB) {
                    SetColor(BLACK); // Black in RGB is dark green in YUV
                    ClearDevice();
                    SSD1926SetRGB(); // Switching shows a little green flicker
                    PlayRGB(records.filename);
                    //DelayMs(2000);
                    // SetState((BUTTON*)GOLFindObject(BTN_VIEW_BACK_SD),BTN_DRAW);
                    //return;
                }
                if (GetFileType(records.filename) == IMAGE_TYPE_CMU) {
                    SetColor(BLACK); // Black in RGB is dark green in YUV
                    ClearDevice();
                    SSD1926SetRGB();
                    putCMUImage(name);
                }
                SetState((BUTTON*) GOLFindObject(BTN_VIEW_BACK_SD), BTN_DRAW);
                return;

            }
        } while (-1 != FindNext(&records));
    }


}

char* sd_get_filelist(unsigned int *numItems) {
    WORD size = 0;
    char *files = NULL;
    SearchRec records;
    *numItems = 0;
    unsigned int fsize = 0;
    if (-1 != FindFirst("*.*", ATTR_ARCHIVE | ATTR_READ_ONLY, &records)) {
        do {
            if (GetFileType(records.filename) != IMAGE_TYPE_OTHER) {
                ++size;

                fsize += FILE_NAME_SIZE_8P3 + 2;
            }
        } while (-1 != FindNext(&records));
        //  fsize=10*size;
        files = (char *) malloc(sizeof (char) *fsize);

    }
    *numItems = size;
    if (-1 != FindFirst("*.*", ATTR_ARCHIVE | ATTR_READ_ONLY, &records)) {
        unsigned int i = 0;
        do {
            if (GetFileType(records.filename) != IMAGE_TYPE_OTHER) {
                //char *name = (char *) malloc(sizeof (char) *strlen(records.filename) + 2);
                unsigned int j = 0;
                while (records.filename[j] != '\0') {
                    files[i] = records.filename[j];
                    i++;
                    j++;
                }
                files[i] = '\n';
                i++;
                j = 0;
            }

        } while (-1 != FindNext(&records));

        files[i] = '\0';
    }
    return files;
}

int32_t sd_delete_image(char *name) {
    SearchRec records;

    if (-1 != FindFirst("*.*", ATTR_ARCHIVE | ATTR_READ_ONLY | ATTR_HIDDEN, &records)) {
        do {

            if (!strcmp(name, records.filename)) {
                if (!FSremove(records.filename))
                    return 1;
            }
        } while (-1 != FindNext(&records));
    }
    return -1;
}

int search_file(char *fname) {
    SearchRec records;

    if (-1 != FindFirst("*.*", ATTR_ARCHIVE | ATTR_READ_ONLY | ATTR_HIDDEN, &records)) {
        do {

            if (!strcmp(fname, records.filename)) {
                return 1;
            }
        } while (-1 != FindNext(&records));
    }
    return 0;
}

/************************************************************
 * WORD StreamRGBFrame(FSFILE *fileStream, WORD numSectors)
 * This is a hack of the MDD file system to be able to use
 * the SSD1926 SD Card DMA feature.
 ************************************************************/
WORD StreamRGBFrame(FSFILE *fileStream, WORD numSectors) {
    DISK *dsk;
    DWORD sec_sel;
    WORD sectorsToRead;
    DWORD currentCluster;
    DWORD prevousCluster;
    static DWORD add = START_ADD;

    dsk = (DISK *) fileStream->dsk;

    sec_sel = Cluster2Sector(dsk, fileStream->ccls);
    sec_sel += (WORD) fileStream->sec; // add the sector number to it

    currentCluster = fileStream->ccls;
    prevousCluster = currentCluster;

    // This will be the minimum sectors that are available by the card to be read
    sectorsToRead = (WORD) dsk->SecPerClus - (WORD) fileStream->sec;

    // get as many sectors from clusters that are contiguous
    while (sectorsToRead < numSectors) {
        if (FILEget_next_cluster(fileStream, 1) != CE_GOOD)
            return 0xFFFF;

        if ((prevousCluster + 1) != fileStream->ccls) {
            fileStream->ccls = prevousCluster;
            break;
        }

        prevousCluster++;
        sectorsToRead += dsk->SecPerClus;
    }

    // make sure that we are not over the bounds
    if (sectorsToRead > numSectors)
        sectorsToRead = numSectors;

    // do a DMA write of the information from the SD card to the display buffer
    if (!SDSectorDMARead(sec_sel, add, sectorsToRead)) {
        fileStream->ccls = currentCluster;
        return 0;
    } else {
        // update the address
        add += (dsk->sectorSize * (DWORD) sectorsToRead);

        if (add >= (FRAME_SIZE + START_ADD)) {
            add = START_ADD;
        }

        // update the pointers
        fileStream->seek += (dsk->sectorSize * sectorsToRead);

        if (fileStream->seek > fileStream->size) {
            fileStream->seek = fileStream->size;
            return 0xFFFF;
        }
    }

    // get the current sector within the current cluster
    currentCluster = fileStream->sec + sectorsToRead;
    while (currentCluster > dsk->SecPerClus)
        currentCluster -= dsk->SecPerClus;

    fileStream->sec = currentCluster;

    // get a new cluster if necessary
    if (fileStream->sec == dsk->SecPerClus) {
        fileStream->sec = 0;
        if (FILEget_next_cluster(fileStream, 1) != CE_GOOD)
            return 0xFFFF;
    }

    return sectorsToRead;

}

/************************************************************
 * void PlayRGB(const char * fileName)
 ************************************************************/
void PlayRGB(const char * fileName) {

    DWORD total_frames;
    WORD sectorsToWrite;
    FSFILE *pFile = NULL;
    int i;
    BYTE reg;

    reg = GetReg(REG_SPECIAL_EFFECTS);
    rgbReg = reg;
    SetReg(REG_SPECIAL_EFFECTS, reg & 0xBF);

    pFile = FSfopen(fileName, "rb");
    rgbFile = pFile;
    total_frames = (pFile->size / FRAME_SIZE);

    SetColor(BLACK);
    ClearDevice();

    DisplayEnable(); // Aseert chip select to SSD1926

    for (i = 0; i < total_frames; i++) {
        sectorsToWrite = FRAME_SIZE / pFile->dsk->sectorSize;

        while (sectorsToWrite) {
            WORD result = StreamRGBFrame(pFile, sectorsToWrite);

            if (result == 0xFFFF)
                break;

            sectorsToWrite -= result;
        }
    }

}

void getrand_string(char *str, int8_t len) {

    short int i;
    for (i = 0; i < len; ++i) {
        str[i] = alphanum[(100 * len * next_val()) * next_val() % (sizeof (alphanum) - 1)];
    }

}

void getFileName(char *fname, IMAGE_TYPE type) {

    getrand_string(fname, 8);
    fname[8] = '.';
    if (type == IMAGE_TYPE_JPG) {
        fname[9] = 'J';
        fname[10] = 'P';
        fname[11] = 'G';
    } else if (type == IMAGE_TYPE_RGB) {
        fname[9] = 'R';
        fname[10] = 'G';
        fname[11] = 'B';
    } else if (type == IMAGE_TYPE_CMU) {
        fname[9] = 'C';
        fname[10] = 'M';
        fname[11] = 'U';
    }


}

int16_t next_val() {
    static int16_t lim = 25421;
    static long a = 1;
    // could be made the seed value
    a = (a * 32719 + 3) % 32749;
    int16_t r = ((a % lim) + 1);
    return r;

}

void putCMUImage(char *fname) {

    short int i, j;
    unsigned char xlen, ylen;
    BYTE rgb[3];
    FSFILE *fp;
    if ((fp = FSfopen(fname, FS_READ)) == NULL) {
        return;
    }
    FSfread(&xlen, 1, 1, fp);
    while (xlen != 87)
        FSfread(&xlen, 1, 1, fp);
    FSfread(&ylen, 1, 1, fp);
    if (xlen <= 0 || ylen <= 0)
        return;

    for (i = 0; i <= ylen * 2 - 1; i += 2) {
        for (j = 0; j <= xlen * 3 - 2; j += 3) {

            if (!FSfeof(fp))
                FSfread(rgb, 1, 3, fp);

            SetColor(RGBConvert(rgb[0], rgb[1], rgb[2]));
            PutPixel(j, i);
            PutPixel(j, i + 1);
            PutPixel(j + 1, i);
            PutPixel(j + 1, i + 1);
            PutPixel(j + 2, i);
            PutPixel(j + 2, i + 1);
        }
        SetColor(BLACK);
        while (j <= GetMaxX()) {
            PutPixel(j, i);
            ++j;
        }
    }
    SetColor(BLACK);
    while (i < GetMaxY()) {
        for (j = 0; j < GetMaxX(); j++) {
            PutPixel(j, i);
        }
        ++i;
    }
    FSfclose(fp);
}