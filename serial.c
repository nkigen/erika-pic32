
#include <p32xxxx.h>
#include <plib.h>

// Configuration Bit settings
// SYSCLK = 80 MHz (8MHz Crystal/ FPLLIDIV * FPLLMUL / FPLLODIV)
// PBCLK = 40 MHz
// Primary Osc w/PLL (XT+,HS+,EC+PLL)
// WDT OFF
// Other options are don't care
#pragma config FPLLMUL = MUL_20, FPLLIDIV = DIV_2, FPLLODIV = DIV_1, FWDTEN = OFF
#pragma config POSCMOD = HS, FNOSC = PRIPLL, FPBDIV = DIV_1

#define SYS_FREQ 				(80000000L)
#define DESIRED_BAUDRATE    	(115200)      //The desired BaudRate
#define MAX_WIDTH 176
#define MAX_HEIGHT 144
#define MM 10
#define FRAME_BUFFER_SIZE MAX_HEIGHT*MAX_WIDTH
#define NULL_CHAR '\0'
#define READY_CHAR ':'
#define MAX_ATTEMPTS 5

typedef enum {
    RESET,
    IDLE,
    FRAME_STREAM,
    SEND_FRAME,
    GET_VERSION,
    NUM_COMMANDS

} CMUCam2_COMMAND;
static const char CMUCam2Commands[NUM_COMMANDS][4] = {
    [RESET] = "rs\r",
    [GET_VERSION] = "gv\r",
    [IDLE] = " \r",
    [SEND_FRAME] = "sf\r",
    [FRAME_STREAM] = "fs1\r"
};

static enum {
    FIRST_CHAR = 0,
    FILL_BUFFER
} FIRST_CHAR_SM = FIRST_CHAR;

static enum {
    A = 0,
    AC,
    ACK,
    INIT
} GOOD_ACK = A;

static enum {
    N = 0,
    NC,
    NCK,
    BAD

} BAD_ACK = N;

/*global variables*/

CMUCam2_COMMAND currentCommand; /*last command send to the camera*/
int resendCommand = 1; /*1-transmission successful, 0 transmission error*/
int pbClk; /*TODO:define pbclk in main*/
int process_command = 0; /*Wait for RX interrupt to be processsed*/
char frameBuffer[FRAME_BUFFER_SIZE]; /*stores the frame buffer*/
UINT8 width, height; //real image width and height as read from camera

/*Function prototypes*/
void get_frame_packet();
void cmucam2_init_comm();
int send_command(CMUCam2_COMMAND cmd);
void read_camera_frame();
void empty_U2RXREG_upto(const char c);

void DelayMs(unsigned int msec) {
    unsigned int tWait, tStart;

    tWait = (SYS_FREQ / 2000) * msec; //    SYS_FREQ        (80000000)
    tStart = ReadCoreTimer();
    while ((ReadCoreTimer() - tStart) < tWait);
}

void cmucam2_init_comm() {
    // Open UART2 COM port
    OpenUART2(UART_EN, // Module is ON
            UART_RX_ENABLE | UART_TX_ENABLE, // Enable TX & RX
            pbClk / 16 / DESIRED_BAUDRATE - 1); // 9600 bps, 8-N-1
    // Configure UART2 RX Interrupt
    ConfigIntUART2(UART_INT_PR2 | UART_RX_INT_EN);
    // Must enable global interrupts (multi-vector mode)
    INTEnableSystemMultiVectoredInt();
}

int main(void) {
  //  int pbClk;
    pbClk = SYSTEMConfig(SYS_FREQ, SYS_CFG_WAIT_STATES | SYS_CFG_PCACHE);

    cmucam2_init_comm();
    while (1) {
        send_command(RESET);
        send_command(FRAME_STREAM);
        send_command(SEND_FRAME);
       // read_camera_frame();
    }
    return 0;
}


// UART 2 interrupt handler
// it is set at priority level 2

void __ISR(_UART2_VECTOR, ipl2) IntUart2Handler(void) {
    /**SM for filling the buffer*/
    process_command = 1;
    // Is this an RX interrupt?
    if (mU2RXGetIntFlag()) {
        // Clear the RX interrupt Flag
        mU2RXClearIntFlag();
        if(currentCommand==SEND_FRAME)
            process_command=1;
        while (1) {
            char rx = ReadUART2();
            if (rx == '\0')
                goto EXIT_INT;
            switch (FIRST_CHAR_SM) {
                case FIRST_CHAR:
                    switch (rx) {
                        case 'A': GOOD_ACK++;
                            break;
                        case 'N': BAD_ACK++;
                            break;
                        case 'C': GOOD_ACK++;
                            BAD_ACK++;
                            break;
                        case 'K': GOOD_ACK++;
                            BAD_ACK++;
                            break;
                        default: GOOD_ACK = A;
                            BAD_ACK = N;
                            break;
                    }

                    if (GOOD_ACK == INIT) {
                        GOOD_ACK = A;
                        BAD_ACK = N;
                        resendCommand = 0;
                        if (currentCommand == SEND_FRAME) {
                            FIRST_CHAR_SM = FILL_BUFFER;

                        } else {
                            empty_U2RXREG_upto(NULL_CHAR);
                            goto EXIT_INT;
                        }
                    } else if (BAD_ACK == BAD) {
                        GOOD_ACK = A;
                        BAD_ACK = N;
                        FIRST_CHAR_SM = FIRST_CHAR;
                        resendCommand = 1;
                        empty_U2RXREG_upto(NULL_CHAR);
                        goto EXIT_INT;
                    }
                    break;
                case FILL_BUFFER:
                    FIRST_CHAR_SM = FIRST_CHAR;
                    if (currentCommand == SEND_FRAME) {
                        get_frame_packet();
                        goto EXIT_INT;
                    }
            }
        }
    }

EXIT_INT: // We don't care about TX interrupt
    if (mU2TXGetIntFlag()) {
        mU2TXClearIntFlag();
    }
}

void get_frame_packet() {
   unsigned char red, green, blue;
  
    UINT8 new_frame = 0;
    UINT8 row = 0, col = 0;
    int pix = 0;
    int i = 0;

    for (i = 0; i < FRAME_BUFFER_SIZE; i++)
        frameBuffer[i] = 0;
    empty_U2RXREG_upto(READY_CHAR);
    while (1) {
        char data = ReadUART2();

        if (data == 1) {
            new_frame = 1;
            width = ReadUART2();
            height = ReadUART2();
        } else if (data == 2) {
            if (row < height)
                row++;
            else if (row == height)
                break;

        } else if (data == 3) {
            /**TODO: process array*/
           // send_command(IDLE);
            return;
        } else if (new_frame) {
            red = data;
            green = ReadUART2();
            blue = ReadUART2();
            col++;
            pix = 0;
            pix += blue;
            pix += green << 8;
            pix += red << 16;

            frameBuffer[col + row - 2] = pix;
        }

    }
}

/**
 *
 * @param cmd
 * @return int
 */
int send_command(CMUCam2_COMMAND cmd) {
    int try = MAX_ATTEMPTS;
    currentCommand = cmd;
    resendCommand=1;
    while (resendCommand && try) {
        putsUART2(CMUCam2Commands[cmd]);
        process_command=0;
        while (!process_command); //wait for RX interrupt to be called

        --try;
        //uu
    }
    if (!try)
        return 0;
    return 1;
}

void read_camera_frame() {

    if (send_command(FRAME_STREAM)) {
        if (send_command(SEND_FRAME)) {

        } else {

        }
    } else {

    }

}

/**
 * 
 * @param c
 */
void empty_U2RXREG_upto(const char c) {
    char d = ' ';
    d = ReadUART2();
    while (DataRdyUART2())
        d = ReadUART2();
    return;

}


