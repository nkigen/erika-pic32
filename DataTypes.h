/* 
 * File:   DataTypes.h
 * Author: nkigen
 *
 * Created on 21 April 2013, 19:17
 *
 * todo
 * implement the delay function
 *
 */

#ifndef DATATYPES_H
#define	DATATYPES_H

#include <stdint.h>
#include<plib.h> 
#define MAX_FNAME 12

typedef enum {
    IMAGE_TYPE_JPG, IMAGE_TYPE_RGB, IMAGE_TYPE_CMU, IMAGE_TYPE_OTHER
} IMAGE_TYPE;

#endif	/* DATATYPES_H */

