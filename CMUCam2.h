/**
 * \file   CMUCam2.h
 * \author nkigen
 *
 * Created on 28 May 2013, 21:57
 */

#ifndef CMUCAM2_H
#define	CMUCAM2_H
#include "ErrorCodes.h"
/**
 * \def SYS_FREQ
 * \brief The frequency of the system clock
 */
#define SYS_FREQ 				(80000000L)
/**
 * \def DESIRED_BAUDATE
 * \brief The desired baudrate for use with UART
 * This is the baudrate used to communicate with the CMUCam2 using UART2 port
 */
#define DESIRED_BAUDRATE    	(115200)      //The desired BaudRate
//#define MAX_CAMERA_MSG_LEN 150

/**
 * \def MAX_WIDTH
 * \brief The maximum width of the image gotten from the camera
 *
 */
#define MAX_WIDTH 143

/**
 * \def MAX_HEIGHT
 * \brief The maximum height of the image gotten from the camera
 *
 */
#define MAX_HEIGHT 87
#define MM 10

/**
 * \def FRAME_BUFFER_SIZE
 * \brief Size of the buffer to store the image pixel values
 * Calculated as the product of the height and width of the image
 *
 */
#define FRAME_BUFFER_SIZE (MAX_HEIGHT*MAX_WIDTH)*3+2

/**
 * \def NULL_CHAR
 * \brief Null character '\0'
 */
#define NULL_CHAR '\0'

/**
 * \def READY_CHAR
 * \brief Ready indication from the camera
 * When recieved from the camera, it indicates that the CMUCam2 is ready for the next command
 */
#define READY_CHAR ':'

/**
 * \def MAX_ATTEMPTS
 * \brief Maximum number of attempts to send a command to the camera
 */
#define MAX_ATTEMPTS 1

/**
 * \def MOE_DETECT
 * \brief Connection mode with the camera
 * In this mode, the application is trying to establish a connection with the camera
 */
#define MODE_DETECT 0

/**
 * \def MOE_DETECT
 * \brief Image streaming mode
 * In this mode, the application is trying to stream an image from the camera
 */
#define MODE_PICTURE 1

/**
 * \def MAX_BEFORE_RESET
 * \brief Maximum amount of same characters received from the camera a reset is made
 * Specifies the maximum number of same characters to be read from the camera before a rest is made.
 * This is used when trying to empty the RX buffer of the UART2 port.
 */
#define MAX_BEFORE_RESET 5

#define MAX_TIMEOUT_ACK 50

#include<plib.h>


/*Camera status messages*/

/**
 * 
 */
typedef enum {
    RESET,
    GET_VERSION,
    IDLE,
    SEND_FRAME,
    FRAME_STREAM,
    NUM_COMMANDS

} CMUCam2_COMMAND;

/**
 *
 */
static const char CMUCam2Commands[NUM_COMMANDS][4] = {
    [RESET] = "rs\r",
    [GET_VERSION] = "gv\r",
    [IDLE] = " \r",
    [SEND_FRAME] = "sf\r",
    [FRAME_STREAM] = "fs1\r"
};

/**
 *
 */
typedef enum {
    STATUS_INIT, //
    STATUS_IDLE,
    STATUS_ERROR,
            STATUS_TEST,
    STATUS_FETCH,
    STATUS_STREAM,
    STATUS_RESET,
    STATUS_OFF,
    STATUS_SAVE

} CAMERA_STATUS;


/*global variables*/
//char cameraStatusMsg[MAX_CAMERA_MSG_LEN];
extern CMUCam2_COMMAND currentCommand; /*last command send to the camera*/
extern CAMERA_STATUS camStatus; 
extern int pbClk; /*TODO:define pbclk in main*/
extern int process_command; /*Wait for RX interrupt to be processsed*/
extern unsigned char frameBuffer[FRAME_BUFFER_SIZE]; /*stores the frame buffer*/
extern UINT8 width, height; //real image width and height as read from camera
extern int pbClk;
extern WORD errorBuffer;
extern WORD camStatusBuffer;
extern char CamErrorGettingImage[];
extern int bindex;
/*******/

/*Function prototypes*/

void cmucam2_init_comm();
int send_command(CMUCam2_COMMAND cmd);
//void read_camera_frame(); 
extern void DelayMs(UINT16 ms);
void save_image(void);

/*Camera function commands*/
char *set_status(CAMERA_STATUS cs);
int camera_detect();
int fetch_image(int num);
//int reset_camera();

#endif	/* CMUCAM2_H */

