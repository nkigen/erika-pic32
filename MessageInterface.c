#include "MessageInterface.h"
#include "GOL.h"
#include "StaticText.h"
#include<string.h>
#include<stdio.h> 
XCHAR mainBool = 1; //Update widgets in main screen
XCHAR sdBool = 1; // boolean for updating widgets in sd screen
XCHAR imgdelBool = 1;
XCHAR imgViewBool = 1;
XCHAR cameraBool = 1;
WORD messageBuffer = 0x000;
WORD errorBuffer = 0x000;
XCHAR *xcharStringPtr = NULL;
XCHAR fileToDelete[12]; //name of file to be deleted stored here
XCHAR fileToView[12];
int errorInfo = -1;
int refreshDevices = 0;
char hardwareBuffer[2]={FALSE,FALSE};
/**
 * 
 */

void updateWidgets() {
    // if (currentGDDDemoScreenIndex ==DELETE_IMG_SCREEN && imgdelBool)
    //  update_del_image_screen_widgets();

    /*check current screen adn call coresponding update function */
    ActivateTask(TaskHWDetect);
    if (currentGDDDemoScreenIndex == VIEW_IMAGE_SCREEN && imgViewBool)
        update_image_view_screen();
    if (currentGDDDemoScreenIndex == SD_SCREEN && SDDetect() == FALSE) {
        GDDDemoGoToScreen(0);
        return;
    }
    if (currentGDDDemoScreenIndex == SD_SCREEN && sdBool)
        update_sd_screen_widgets();
    if (currentGDDDemoScreenIndex == MAIN_SCREEN) {
        update_main_widgets();
    }
    if (currentGDDDemoScreenIndex == SD_SCREEN && (CHECK_MESSAGE(messageBuffer, BTN_NO_DELETE) || CHECK_MESSAGE(messageBuffer, BTN_CONFIRM_DEL)))
        update_sd_screen_widgets();
    if (currentGDDDemoScreenIndex == CAMERA_SCREEN)
        update_camera_screen();

    /*enable previously disabled buttons*/
    if (currentGDDDemoScreenIndex == SD_SCREEN) {

        LISTITEM *lst = LbGetSel((LISTBOX*) GOLFindObject(LSB_3), NULL);
        if (lst != NULL) {
            if (GetState((BUTTON*) GOLFindObject(BTN_OPEN_IMG), BTN_DISABLED))
                ClrState((BUTTON*) GOLFindObject(BTN_OPEN_IMG), BTN_DISABLED);
            if (GetState((BUTTON*) GOLFindObject(BTN_DELETE_IMG), BTN_DISABLED))
                ClrState((BUTTON*) GOLFindObject(BTN_DELETE_IMG), BTN_DISABLED);
        }

    }
    return;

}

void update_camera_screen() {
    /**TODO: ST_HIDE enable/disable*/
    static int stat_refresh=10;
    if (bindex > 0) {
        PROGRESSBAR *stat = (PROGRESSBAR*) GOLFindObject(PRB_FETCH);
        if (stat != NULL) {
            WORD pos = (WORD) (((bindex + 1)*100) / FRAME_BUFFER_SIZE);
            PbSetPos(stat, pos);
            //if((FRAME_BUFFER_SIZE%bindex)==0)
            SetState(stat,  PB_DRAW_BAR);
        }
    }

    if (CHECK_MESSAGE(messageBuffer, BTN_INIT_CAM)) {
        process_get_image(1);
        //  process_get_image(1);
        TOGGLE_MESSAGE(messageBuffer, BTN_INIT_CAM);
    }
    if (CHECK_MESSAGE(camStatusBuffer, STATUS_SAVE)) {
        save_image();
        StSetText((STATICTEXT*) GOLFindObject(STE_14), set_status(STATUS_SAVE));
        SetState((STATICTEXT*) GOLFindObject(STE_14), ST_DRAW);
        TOGGLE_MESSAGE(camStatusBuffer, STATUS_SAVE);
    }
    if (CHECK_MESSAGE(camStatusBuffer, STATUS_FETCH)) {
        StSetText((STATICTEXT*) GOLFindObject(STE_14), set_status(STATUS_FETCH));
        SetState((STATICTEXT*) GOLFindObject(STE_14), ST_DRAW);
        TOGGLE_MESSAGE(camStatusBuffer, STATUS_FETCH);
    }
    if (CHECK_MESSAGE(camStatusBuffer, STATUS_SAVE)) {
        StSetText((STATICTEXT*) GOLFindObject(STE_14), set_status(STATUS_SAVE));
        SetState((STATICTEXT*) GOLFindObject(STE_14), ST_DRAW);
        TOGGLE_MESSAGE(camStatusBuffer, STATUS_SAVE);
    }
    if (CHECK_MESSAGE(errorBuffer, ERROR_SAVE_IMAGE)) {
        StSetText((STATICTEXT*) GOLFindObject(STE_14), set_status(STATUS_ERROR));
        SetState((STATICTEXT*) GOLFindObject(STE_14), ST_DRAW);
        TOGGLE_MESSAGE(errorBuffer, ERROR_SAVE_IMAGE);
    }

    cameraBool = 1;
    sdBool = 1;
    mainBool = 1;
    imgViewBool = 1;
    imgdelBool = 1;
}

void update_sd_screen_widgets() {

//     if (SDDetect() == FALSE) {
  if(hardwareBuffer[0]==FALSE){
        GDDDemoGoToScreen(0);
        return;
    }
    if (CHECK_MESSAGE(messageBuffer, BTN_2)) {
        update_lsb_3();
        TOGGLE_MESSAGE(messageBuffer, BTN_2);
        //return;
    }
    if (CHECK_MESSAGE(messageBuffer, BTN_NO_DELETE)) {
        update_lsb_3();
        TOGGLE_MESSAGE(messageBuffer, BTN_NO_DELETE);
        //return;
    }
    if (CHECK_MESSAGE(messageBuffer, BTN_CONFIRM_DEL)) {
        update_lsb_3();
        TOGGLE_MESSAGE(messageBuffer, BTN_CONFIRM_DEL);
        //return;
    }

    if (CHECK_MESSAGE(messageBuffer, BTN_VIEW_BACK_SD)) {
        update_lsb_3();
        TOGGLE_MESSAGE(messageBuffer, BTN_VIEW_BACK_SD);
        //return;
    }
    sdBool = 0;
    mainBool = 1;
    imgViewBool = 1;
    cameraBool = 1;
    imgdelBool = 1;
}

void update_main_widgets() {

    if (CHECK_MESSAGE(messageBuffer, BTN_CAM)) {
        process_get_image(0);
        TOGGLE_MESSAGE(messageBuffer, BTN_CAM);
    }


//     BYTE chk = SDDetect();
BYTE chk=hardwareBuffer[0];

    //sd_init();
    //Check SD card status
    if (chk == FALSE) {
        SDStatus = SD_ABSENT;
        if (!GetState((BUTTON*) GOLFindObject(BTN_2), BTN_DISABLED)) {
            SetState((BUTTON*) GOLFindObject(BTN_2), BTN_DISABLED);
            SetState((BUTTON*) GOLFindObject(BTN_2), BTN_DRAW);

            StSetText((STATICTEXT*) GOLFindObject(STE_SD_STATUS), setSDStatus());
            SetState((STATICTEXT*) GOLFindObject(STE_SD_STATUS), ST_DRAW);
        }
        initSD = TRUE;

    } else if (chk == TRUE) {
        if (initSD)
            sd_init();
        SDStatus = SD_PRESENT;
        if (GetState((BUTTON*) GOLFindObject(BTN_2), BTN_DISABLED)) {
            ClrState((BUTTON*) GOLFindObject(BTN_2), BTN_DISABLED);
            SetState((BUTTON*) GOLFindObject(BTN_2), BTN_DRAW);
        }

        StSetText((STATICTEXT*) GOLFindObject(STE_SD_STATUS), setSDStatus());
        SetState((STATICTEXT*) GOLFindObject(STE_SD_STATUS), ST_DRAW);
    }

    // */
    /*Check Camera Status*/
//     int ret = camera_detect();
int ret=hardwareBuffer[1];
    if (ret) {
        if (GetState((BUTTON*) GOLFindObject(BTN_CAM), BTN_DISABLED)) {
            ClrState((BUTTON*) GOLFindObject(BTN_CAM), BTN_DISABLED);
            SetState((BUTTON*) GOLFindObject(BTN_CAM), BTN_DRAW);
        }
        StSetText((STATICTEXT*) GOLFindObject(STE_16), set_status(STATUS_INIT));
        SetState((STATICTEXT*) GOLFindObject(STE_16), ST_DRAW);
    } else {
        if (!GetState((BUTTON*) GOLFindObject(BTN_CAM), BTN_DISABLED)) {
            SetState((BUTTON*) GOLFindObject(BTN_CAM), BTN_DISABLED | BTN_DRAW);
            // SetState((BUTTON*) GOLFindObject(BTN_CAM), BTN_DRAW);

            StSetText((STATICTEXT*) GOLFindObject(STE_16), set_status(STATUS_OFF));
            SetState((STATICTEXT*) GOLFindObject(STE_16), ST_DRAW);
        }
    }

    sdBool = 1;
    imgdelBool = 1;
    mainBool = 0;
    imgViewBool = 1;
    cameraBool = 1;
}

void update_image_view_screen() {
    if (CHECK_MESSAGE(messageBuffer, BTN_OPEN_IMG)) {
        view_image();
        TOGGLE_MESSAGE(messageBuffer, BTN_OPEN_IMG);
        //return;
    }
    sdBool = 1;
    imgdelBool = 1;
    mainBool = 1;
    imgViewBool = 0;
    cameraBool = 1;
}

void update_lsb_3() {
    LISTBOX *plb = (LISTBOX *) GOLFindObject(LSB_3);
    if (plb == NULL)
        return;
    LbDelItemsList(plb); //delete all items first
    int n; //number of files
    XCHAR *pText = sd_get_filelist(&n);
    xcharStringPtr = pText;
    AddItemList(pText, plb);

    SetState(plb, LB_DRAW);

}

void AddItemList(XCHAR *pText, LISTBOX *pLb) {
    XCHAR *pointer;
    WORD ctr;

    if (pText != NULL) {
        pointer = pText;
        ctr = 0;
        while (*pointer) {
            if (NULL == LbAddItem(pLb, NULL, pointer, NULL, 0, ctr))
                break;
            while (*pointer++ > 31);
            if (*(pointer - 1) == 0)
                break;
            ctr++;
        }
    }
}

void delete_image() {
    sd_delete_image(fileToDelete);
}

void view_image() {
    sd_open_image(fileToView);
}

void process_get_image(int num) {

    fetch_image(num);

}