This Project controls a CMUCam2 CMOS camera from a touchscreen Interface using the Erika Enterprise RealTime Operating System.

The following Hardware is used:
    - Explorer16 development board with a PIC32MX795F512L microcontroller
    - CMUCam2 ( http://www.cs.cmu.edu/~cmucam2/ )
    - Graphics LCD Controller PICtailTM Plus SSD1926 Board (AC164127-5) , an add-on to the Explorer16 development board
    - 3.2'' DISPLAY KIT (AC164127-3) with touchscreen.

The following software stacks are used:
    - Erika Enterprise RTOS 
    - Microchips Graphics Library 
 
 For the complete documentation, go to https://drive.google.com/file/d/0B4vTPjLX2y2dbGZFQ1VKejViWk0/edit?usp=sharing
		        
