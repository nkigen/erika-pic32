/* 
 * File:   RGB_Encoder.h
 * Author: nkigen
 *
 * Created on 19 July 2013, 14:03
 */

#ifndef RGB_ENCODER_H
#define	RGB_ENCODER_H 
#include "MainDemo.h"


static int putlong(FSFILE *outf, unsigned long val) ;
void putshort(FSFILE *outf, unsigned short val);
void putbyte(FSFILE *outf, unsigned char val);
void writeheader(FSFILE *outf,char *iname,UINT8 IXSIZE, UINT8 IYSIZE);

#endif	/* RGB_ENCODER_H */

