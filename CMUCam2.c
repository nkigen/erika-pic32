#include "CMUCam2.h"
#include "SD.h"
#include "MessageInterface.h"

CMUCam2_COMMAND currentCommand; /*last command send to the camera*/
//int retryCommand = 1; /*1-transmission successful, 0 transmission error*/
int pbClk = 0; /*TODO:define pbclk in main*/
int process_command = 0; /*Wait for RX interrupt to be processsed*/
unsigned char frameBuffer[FRAME_BUFFER_SIZE]; /*stores the frame buffer*/
UINT8 width = 0, height = 0; //real image width and height as read from camera
char CamStatusInit[] = "Camera Detected";
char CamGettingImage[] = "Saving Image...Image Saved!";
char CamErrorGettingImage[] = "Error Getting Image Check Camera...";
char CamReady[] = "Camera ready";
char CamConnect[] = "Connect Camera!";
char CamFetchImage[]="Streaming Image...";
int endOfFrame = 0;
int newFrame = 0;
CAMERA_STATUS camStatus = STATUS_OFF;
WORD camStatusBuffer;


int bindex = 0; //buffer index

void cmucam2_init_comm() {
    // Open UART2 COM port
    OpenUART2(UART_EN, // Module is ON
            UART_RX_ENABLE | UART_TX_ENABLE, // Enable TX & RX
            GetSystemClock() / 16 / DESIRED_BAUDRATE - 1); // 115200 bps, 8-N-1
    // Configure UART2 RX Interrupt
    ConfigIntUART2(UART_INT_PR2 | UART_RX_INT_EN);
    UART_FIFO_MODE(UART_INTERRUPT_ON_RX_NOT_EMPTY);
    // Must enable global interrupts (multi-vector mode)
}

/**
 *
 * @return 1=success, 0=failure
 */
int fetch_image(int num) {

    static int j = 0;
    if (num == 0)
        j = num;
    /*
        if (j == 0) {
            REGISTER_MESSAGE(camStatusBuffer, STATUS_INIT);
            camStatus = STATUS_INIT;
            send_command(IDLE);
        } else */
    if (j == 0) {
        REGISTER_MESSAGE(camStatusBuffer, STATUS_INIT);
        camStatus = STATUS_INIT;
        send_command(FRAME_STREAM);
    } else {
        REGISTER_MESSAGE(camStatusBuffer, STATUS_FETCH);
        camStatus = STATUS_FETCH;
        endOfFrame = 0;
        send_command(SEND_FRAME);
    }
    ++j;
    return 1;
}

void __ISR(_UART2_VECTOR, ipl2) IntUart2Handler(void) {
    process_command = 1;
    unsigned char c;
    if (mU2RXGetIntFlag()) {
        mU2RXClearIntFlag();
    }

    if (bindex == (FRAME_BUFFER_SIZE) || endOfFrame) {
        send_command(IDLE);
        REGISTER_MESSAGE(camStatusBuffer, STATUS_SAVE);
        camStatus = STATUS_SAVE;
        bindex = 0;
        //return;
    } else {
        if (DataRdyUART2()) {
            switch (currentCommand) {
                case IDLE:
                    if (camStatus == STATUS_OFF)
                        camStatus = STATUS_INIT;
                    ReadUART2();
                    break;
                case FRAME_STREAM:
                    ReadUART2();
                    break;
                case SEND_FRAME:
                    camStatus=STATUS_FETCH;
                    c = ReadUART2();
                    if (c == 1) {
                        newFrame = 1;
                        while ((c = ReadUART2()) != 87);
                        frameBuffer[bindex++] = c;
                        while ((c = ReadUART2()) < 16);
                        frameBuffer[bindex++] = c;
                    } else if (c == 3)
                        endOfFrame = 1;
                    else {
                        if (newFrame) {
                            if (c >= 16 && c <= 255)
                                frameBuffer[bindex++] = c;
                        }
                    }

                    break;
                default:
                    //while (DataRdyUART2() == 0);
                    ReadUART2();
                    break;
            }
        }

    }

    /*Check for buffer overrun/overflow*/
    if (U2STAbits.OERR) {
        while (DataRdyUART2()) {
            switch (currentCommand) {
                case IDLE:
                    if (camStatus == STATUS_OFF)
                        camStatus = STATUS_INIT;
                    ReadUART2();
                    break;
                case FRAME_STREAM:
                    ReadUART2();
                    break;
                case SEND_FRAME:
                    c = ReadUART2();
                    if (c == 1) {
                        newFrame = 1;
                        while ((c = ReadUART2()) != 87);
                        frameBuffer[bindex++] = c;
                        while ((c = ReadUART2()) < 16);
                    } else if (c == 3)
                        endOfFrame = 1;
                    else {
                        if (newFrame) {
                            if (c >= 16 && c <= 255)
                                frameBuffer[bindex++] = c;
                        }
                    }
                    break;
                default:ReadUART2();
                    break;
            }
        }
        UART2ClearError(U2STAbits.OERR);
        U2STAbits.OERR = 0;
        while (U2STAbits.OERR != 0);


    }
}

int send_command(CMUCam2_COMMAND cmd) {
    process_command = 0;
    while (BusyUART2());
     currentCommand = cmd;
    putsUART2(CMUCam2Commands[cmd]);
   
       //DelayMs(10);

    if (currentCommand != SEND_FRAME)
        DelayMs(10);

    if (!process_command)
        return 0;
    return 1;

}

int camera_detect() {
    return send_command(IDLE);
}

char *set_status(CAMERA_STATUS cs) {
    switch (cs) {
        case STATUS_ERROR:
            switch (errorInfo) {
                case ERROR_SAVE_IMAGE:
                    return CamErrorGettingImage;
                    break;
                default:
                    return &CamErrorGettingImage[20];
                    break;
            }
            break;
        case STATUS_INIT:
            return CamStatusInit;
            break;
        case STATUS_OFF:
            return CamConnect;
            break;
        case STATUS_FETCH:
            return CamFetchImage;
        case STATUS_SAVE:
            return CamGettingImage; 
            break;

    }
}

void save_image(void) {
    camStatus = STATUS_SAVE;
    char fname[8 + 3 + 2];
    getFileName(fname, IMAGE_TYPE_CMU);
    while (search_file(fname))
        getFileName(fname, IMAGE_TYPE_CMU);
    FSFILE *file;
    if ((file = FSfopen(fname, FS_WRITE)) == NULL) {

        camStatus = STATUS_ERROR;
        errorInfo = ERROR_SAVE_IMAGE;
        REGISTER_MESSAGE(errorBuffer, ERROR_SAVE_IMAGE);
        REGISTER_MESSAGE(camStatusBuffer, STATUS_ERROR);
        return;
    }

    FSfwrite(frameBuffer, FRAME_BUFFER_SIZE, 1, file);
    FSfclose(file);
}
