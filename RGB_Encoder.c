#include "RGB_Encoder.h"

void putbyte(FSFILE *outf, unsigned char val) {

    unsigned char buf[1];

    buf[0] = val;
    FSfwrite(buf, 1, 1, outf);
}

void putshort(FSFILE *outf, unsigned short val) {
    unsigned char buf[2];

    buf[0] = (val >> 8);
    buf[1] = (val >> 0);
    FSfwrite(buf, 2, 1, outf);
}

static int putlong(FSFILE *outf, unsigned long val) {
    unsigned char buf[4];

    buf[0] = (val >> 24);
    buf[1] = (val >> 16);
    buf[2] = (val >> 8);
    buf[3] = (val >> 0);
    return FSfwrite(buf, 4, 1, outf);
}

void writeheader(FSFILE *of,char *iname,UINT8 IXSIZE, UINT8 IYSIZE) {
    int i;
    putshort(of, 474); /* MAGIC               */
    putbyte(of, 0); /* STORAGE is VERBATIM */
    putbyte(of, 1); /* BPC is 1            */
    putshort(of, 2); /* DIMENSION is 2      */
    putshort(of, IXSIZE); /* XSIZE               */
    putshort(of, IYSIZE); /* YSIZE               */
    putshort(of, 1); /* ZSIZE               */
    putlong(of, 0); /* PIXMIN is 0         */
    putlong(of, 255); /* PIXMAX is 255       */
    for (i = 0; i < 4; i++) /* DUMMY 4 bytes       */
        putbyte(of, 0);
    FSfwrite(iname, 80, 1, of); /* IMAGENAME           */
    putlong(of, 0); /* COLORMAP is 0       */
    for (i = 0; i < 404; i++) /* DUMMY 404 bytes     */
        putbyte(of, 0);

}