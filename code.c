/* ###*B*###
 * ERIKA Enterprise - a tiny RTOS for small microcontrollers
 *
 * Copyright (C) 2002-2008  Evidence Srl
 *
 * This file is part of ERIKA Enterprise.
 *
 * ERIKA Enterprise is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation, 
 * (with a special exception described below).
 *
 * Linking this code statically or dynamically with other modules is
 * making a combined work based on this code.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole
 * combination.
 *
 * As a special exception, the copyright holders of this library give you
 * permission to link this code with independent modules to produce an
 * executable, regardless of the license terms of these independent
 * modules, and to copy and distribute the resulting executable under
 * terms of your choice, provided that you also meet, for each linked
 * independent module, the terms and conditions of the license of that
 * module.  An independent module is a module which is not derived from
 * or based on this library.  If you modify this code, you may extend
 * this exception to your version of the code, but you are not
 * obligated to do so.  If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * ERIKA Enterprise is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License version 2 for more details.
 *
 * You should have received a copy of the GNU General Public License
 * version 2 along with ERIKA Enterprise; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA.
 * ###*E*### */

#include <ee.h>
#include "cpu/pic32/inc/ee_irqstub.h"
#include "mcu/microchip_pic32/inc/ee_timer.h"
#include "conf.h"
#include "MessageInterface.h"



/*  Initial Configuration (fuse bits)
 * 
 *  Main Clock       -> SYSCLK = Crystal_Freq / FPLLIDIV * FPLLMUL / FPLLODIV
 *  Peripheral Clock -> PBCLK  = SYSCLK / FPBDIV
 */


/* ************************************************************************** */
/*                                  Macros                                    */
/* ************************************************************************** */

/* Timer definitions	*/
//#define TICK_PERIOD 	20000

/****************GLOBAL VARIABLES******************/
int pbClk;
GOL_MSG msg;
/****************************************************/
unsigned char kitt_i = 0;
unsigned char kitt_seq = 0;
unsigned char kitt_seq_n[4] = { 0, 14, 22, 28 }; /* 14, 8, 6 */
unsigned char kitt_seqs[28] = { 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 
0x40, 0x20, 0x10, 0x08, 0x04, 0x2,
0x0f, 0x1e, 0x3c, 0x78, 0xf0, 0xe1, 0xc3, 0x87, 
0x81, 0x42, 0x24, 0x18, 0x24, 0x42 };


/* ************************************************************************** */
/*                               Functions                                    */
/* ************************************************************************** */

/* Clear the Timer1 interrupt status flag */
void T1_clear(void)
{
  IFS0bits.T1IF = 0;
}


/* This is an ISR Type 2 which is attached to the Timer 1 peripheral IRQ pin
 * The ISR simply calls CounterTick to implement the timing reference
 */
void T1_cbk(void)
{
  /* clear the interrupt source */
  T1_clear();
  
  /* count the interrupts, waking up expired alarms */
  CounterTick(myCounter);
}


/* Program the Timer1 peripheral to raise interrupts */
void T1_program(void)
{
  EE_timer_soft_init(EE_TIMER_1,30000);
  EE_timer_set_callback(EE_TIMER_1,&T1_cbk);
  EE_timer_start(EE_TIMER_1);
}



/* ************************************************************************** */
/*                               Tasks                                	      */
/* ************************************************************************** */
TASK(TaskKitt)
{
  /* Set LEDs new status */
  EE_leds(kitt_seqs[kitt_i++]);
  
  /* Loop sequence if finished */
  if ( (kitt_i >= kitt_seq_n[ kitt_seq + 1 ]) || (kitt_i < kitt_seq_n[ kitt_seq ]) )
    kitt_i = kitt_seq_n[ kitt_seq ];
  //EE_leds_on();
}


// TASK(TaskFlash)
// {
//   static unsigned char my_button_status = 0;
//   
//   /* Change LEDs sequence if button S6 is pressed */
//   if (EE_button_get_S6()) {
//     if (my_button_status == 0) {
//       kitt_seq++;
//       if (kitt_seq > 2)
// 	kitt_seq = 0;
//       kitt_i = kitt_seq_n[ kitt_seq ];
//       my_button_status = 1;
//     }
//   } else {
//     my_button_status = 0;
//   }
// }



TASK(TaskDisplay)
{ 
//  ActivateTask(TaskTouch);
//         ActivateTask(TaskKitt);

 while(1){	
    if (GOLDraw()) 
    {
      updateWidgets(); 
     ActivateTask(TaskTouch); 
    }         
}
}

TASK(TaskTouch)
{
//     if(TouchGetX()!=-1){
        TouchGetMsg(&msg); // Get message from touch screen 
	GOLMsg(&msg); // Process message  
//     }
}

TASK(TaskHWDetect){
//   GetResource(RES_HWSTATUS);
  hardwareBuffer[0]=SDDetect();
  hardwareBuffer[1]=camera_detect();
//   ReleaseResource(RES_HWSTATUS);
}

void my_button(void)
{
  /* Light up all LEDs */
  if (EE_button_get_S3() == 1)
    EE_leds_on();
}



/* ************************************************************************** */
/*                                  MAIN                                      */
/* ************************************************************************** */
int main(void)
{
  /* */
  EE_system_init();
  InitializeBoard();
  
  /* Init leds */
  EE_leds_init();
  
  /* Init devices */
  EE_buttons_init(&my_button, 0x01);
  
  /* Program Timer 1 to raise interrupts */
  T1_program();

    cmucam2_init_comm();
  sd_init();
  GDDDemoCreateFirstScreen();

  
  /* Program cyclic alarms which will fire after an initial offset, and after that periodically */
  //SetRelAlarm(AlarmFlash, 10,  10);
  SetRelAlarm(AlarmKitt,  50, 100);
  ActivateTask(TaskDisplay);
   



//  SetRelAlarm(AlarmDisplay,   0, 0);
 // SetRelAlarm(AlarmTouch,   10, 30);
//    SetRelAlarm(AlarmTouch, 10, 10);

  
  /* Forever loop: background activities (if any) should go here */
// EE_analog_init();

  
  for(;;);
  
  
  return 0;
}



/////////////////////////////////////////////////////////////////////////////
// Function: WORD GOLMsgCallback(WORD objMsg, OBJ_HEADER* pObj, GOL_MSG* pMsg)
// Input: objMsg - translated message for the object,
//        pObj - pointer to the object,
//        pMsg - pointer to the non-translated, raw GOL message
// Output: if the function returns non-zero the message will be processed by default
// Overview: it's a user defined function. GOLMsg() function calls it each

//           time the valid message for the object received
/////////////////////////////////////////////////////////////////////////////

WORD GOLMsgCallback(WORD objMsg, OBJ_HEADER *pObj, GOL_MSG *pMsg) {
  WORD objectID;
  
  objectID = GetObjID(pObj);
  
  GDDDemoGOLMsgCallback(objMsg, pObj, pMsg);
  
  // Add additional code here...
  
  return (1);
}

/////////////////////////////////////////////////////////////////////////////
// Function: WORD GOLDrawCallback()
// Output: if the function returns non-zero the draw control will be passed to GOL
// Overview: it's a user defined function. GOLDraw() function calls it each
//           time when GOL objects drawing is completed. User drawing should be done here.
//           GOL will not change color, line type and clipping region settings while

//           this function returns zero.
/////////////////////////////////////////////////////////////////////////////

WORD GOLDrawCallback(void) {
  GDDDemoGOLDrawCallback();
  
  // Add additional code here...
  
  return (1);
}


/////////////////////////////////////////////////////////////////////////////
// Function: Timer3 ISR
// Input: none
// Output: none
// Overview: increments tick counter. Tick is approx. 1 ms.
/////////////////////////////////////////////////////////////////////////////
#ifdef __PIC32MX__
#define __T3_ISR    __ISR(_TIMER_3_VECTOR, ipl4)
#else
#define __T3_ISR    __attribute__((interrupt, shadow, auto_psv))
#endif

/* */
//  static int checkDev = 0;
void __T3_ISR _T3Interrupt(void) {
  
  //refreshDevices=0;
  TMR3 = 0;
  // Clear flag
  #ifdef __PIC32MX__
  mT3ClearIntFlag();
  #else
  IFS0bits.T3IF = 0;
  #endif
  /*
   *    if(checkDev==10)
   *    {
   *        checkDev=0;
   *        refreshDevices=1;
   }
   else
     ++checkDev;*/
  TouchDetectPosition();
   }
   
   /////////////////////////////////////////////////////////////////////////////
   // Function: void TickInit(void)
   // Input: none
   // Output: none
   // Overview: Initilizes the tick timer.
   /////////////////////////////////////////////////////////////////////////////
   
   /*********************************************************************
    * Section: Tick Delay
    *********************************************************************/
   #define SAMPLE_PERIOD       500 // us
   #define TICK_PERIOD			(GetPeripheralClock() * SAMPLE_PERIOD) / 4000000
   
   /* */
   void TickInit(void) {
     
     // Initialize Timer4
     #ifdef __PIC32MX__
     OpenTimer3(T3_ON | T3_PS_1_8, TICK_PERIOD);
     ConfigIntTimer3(T3_INT_ON | T3_INT_PRIOR_4);
     #else
     TMR3 = 0;
     PR3 = TICK_PERIOD;
     IFS0bits.T3IF = 0; //Clear flag
     IEC0bits.T3IE = 1; //Enable interrupt
     T3CONbits.TON = 1; //Run timer
     #endif
     
   }
   
   
   
   /////////////////////////////////////////////////////////////////////////////
   // Function: InitializeBoard()
   // Input: none
   // Output: none
   // Overview: Initializes the hardware components including the PIC device
   //           used.
   /////////////////////////////////////////////////////////////////////////////
   
   void InitializeBoard(void) {
     
     #if defined (PIC24FJ256DA210_DEV_BOARD) && defined(USE_KEYBOARD)
     
     ANSA = 0x0000;
     ANSB = 0x0020; // RB5 as potentiometer input
     ANSC = 0x0010; // RC4 as touch screen X+, RC14 as external source of secondary oscillator
     ANSD = 0x0000;
     ANSE = 0x0000; // RE9 used as S2
     ANSF = 0x0000;
     ANSG = 0x0080; // RG8 used as S1, RG7 as touch screen Y+
     
     #else
     /////////////////////////////////////////////////////////////////////////////
     // ADC Explorer 16 Development Board Errata (work around 2)
     // RB15 should be output
     /////////////////////////////////////////////////////////////////////////////
     #ifndef MEB_BOARD
     LATBbits.LATB15 = 0;
     TRISBbits.TRISB15 = 0;
     #endif
     #endif
     
     
     #ifdef MEB_BOARD
     CPLDInitialize();
     CPLDSetGraphicsConfiguration(GRAPHICS_HW_CONFIG);
     CPLDSetSPIFlashConfiguration(SPI_FLASH_CHANNEL);
     #endif // #ifdef MEB_BOARD
     
     #if defined(__dsPIC33F__) || defined(__PIC24H__) || defined(__dsPIC33E__) || defined(__PIC24E__)
     
     // Configure Oscillator to operate the device at 40Mhz
     // Fosc= Fin*M/(N1*N2), Fcy=Fosc/2
     #if defined(__dsPIC33E__) || defined(__PIC24E__)
     //Fosc = 8M * 60/(2*2) = 120MHz for 8M input clock
     PLLFBD = 58; // M=60
     #else
     // Fosc= 8M*40(2*2)=80Mhz for 8M input clock
     PLLFBD = 38; // M=40
     #endif
     CLKDIVbits.PLLPOST = 0; // N1=2
     CLKDIVbits.PLLPRE = 0; // N2=2
     OSCTUN = 0; // Tune FRC oscillator, if FRC is used
     
     // Disable Watch Dog Timer
     RCONbits.SWDTEN = 0;
     
     // Clock switching to incorporate PLL
     __builtin_write_OSCCONH(0x03); // Initiate Clock Switch to Primary
     
     // Oscillator with PLL (NOSC=0b011)
     __builtin_write_OSCCONL(0x01); // Start clock switching
     while (OSCCONbits.COSC != 0b011);
     
     // Wait for Clock switch to occur
     // Wait for PLL to lock
     while (OSCCONbits.LOCK != 1) {
     };
     
     #if defined(__dsPIC33F__) || defined(__PIC24H__)
     // Set PMD0 pin functionality to digital
     AD1PCFGL = AD1PCFGL | 0x1000;
     
     #if defined(__dsPIC33FJ128GP804__) || defined(__PIC24HJ128GP504__)
     AD1PCFGLbits.PCFG6 = 1;
     AD1PCFGLbits.PCFG7 = 1;
     AD1PCFGLbits.PCFG8 = 1;
     #endif
     
     #elif defined(__dsPIC33E__) || defined(__PIC24E__)
     ANSELE = 0x00;
     ANSELDbits.ANSD6 = 0;
     
     // Set all touch screen related pins to Analog mode.
     ANSELBbits.ANSB11 = 1;
     #endif
     
     #elif defined(__PIC32MX__)
     INTEnableSystemMultiVectoredInt();
     pbClk = SYSTEMConfigPerformance(GetSystemClock());
     #endif // #if defined(__dsPIC33F__) || defined(__PIC24H__)
     
     
     #if defined (EXPLORER_16)
     /************************************************************************
      * For Explorer 16 RD12 is connected to EEPROM chip select.
      * To prevent a conflict between this EEPROM and SST25 flash
      * the chip select of the EEPROM SPI should be pulled up.
      ************************************************************************/
     // Set IOs directions for EEPROM SPI
     MCHP25LC256_CS_LAT = 1; // set initial CS value to 1 (not asserted)
     MCHP25LC256_CS_TRIS = 0; // set CS pin to output
     #endif // #if defined (EXPLORER_16)
     
     // Initialize graphics library and create default style scheme for GOL
     GOLInit();
     
     // Set the other chip selects to a known state
     #ifdef MIKRO_BOARD
     // SD Card chip select
     LATGbits.LATG9 = 1;
     TRISGbits.TRISG9 = 0;
     
     // MP3 Codac
     // reset
     LATAbits.LATA5 = 0;
     TRISAbits.TRISA5 = 0;
     // chip select
     LATAbits.LATA2 = 1;
     TRISAbits.TRISA2 = 0;
     // chip select
     LATAbits.LATA3 = 1;
     TRISAbits.TRISA3 = 0;
     
     AD1PCFGbits.PCFG11 = 1;
     AD1PCFGbits.PCFG10 = 1;
     #endif
     
     //The following are PIC device specific settings for the SPI channel
     //used.
     
     //Set IOs directions for SST25 SPI
     #if defined (GFX_PICTAIL_V3) || defined (MEB_BOARD) || defined(GFX_PICTAIL_LCC) || defined(MIKRO_BOARD) || defined(GFX_PICTAIL_V3E)
     
     SST25_CS_LAT = 1;
     SST25_CS_TRIS = 0;
     
     #ifndef __PIC32MX__
     SST25_SCK_TRIS = 0;
     SST25_SDO_TRIS = 0;
     SST25_SDI_TRIS = 1;
     #if defined(__PIC24FJ256GB210__) || defined(__dsPIC33E__) || defined(__PIC24E__)
     SST25_SDI_ANS = 0;
     #endif
     #endif
     #elif defined (PIC24FJ256DA210_DEV_BOARD)
     SST25_CS_LAT = 1;
     SST25_CS_TRIS = 0;
     
     // Set the pins to be digital
     SST25_SDI_ANS = 0;
     SST25_SDO_ANS = 0;
     
     SST25_SCK_TRIS = 0;
     SST25_SDO_TRIS = 0;
     SST25_SDI_TRIS = 1;
     
     #endif
     
     // set the peripheral pin select for the PSI channel used
     #if defined(__dsPIC33FJ128GP804__) || defined(__PIC24HJ128GP504__)
     AD1PCFGL = 0xFFFF;
     RPOR9bits.RP18R = 11; // assign RP18 for SCK2
     RPOR8bits.RP16R = 10; // assign RP16 for SDO2
     RPINR22bits.SDI2R = 17; // assign RP17 for SDI2
     #elif defined(__PIC24FJ256GB110__) || defined(__PIC24FJ256GA110__) || defined (__PIC24FJ256GB210__)
     __builtin_write_OSCCONL(OSCCON & 0xbf); // unlock PPS
     RPOR10bits.RP21R = 11; // assign RP21 for SCK2
     RPOR9bits.RP19R = 10; // assign RP19 for SDO2
     RPINR22bits.SDI2R = 26; // assign RP26 for SDI2
     __builtin_write_OSCCONL(OSCCON | 0x40); // lock   PPS
     #elif defined(__PIC24FJ256DA210__)
     
     __builtin_write_OSCCONL(OSCCON & 0xbf); // unlock PPS
     
     #if (SST25_SPI_CHANNEL == 1)
     RPOR1bits.RP2R = 8; // assign RP2 for SCK1
     RPOR0bits.RP1R = 7; // assign RP1 for SDO1
     RPINR20bits.SDI1R = 0; // assign RP0 for SDI1
     #elif (SST25_SPI_CHANNEL == 2)
     RPOR1bits.RP2R = 11; // assign RP2 for SCK2
     RPOR0bits.RP1R = 10; // assign RP1 for SDO2
     RPINR22bits.SDI2R = 0; // assign RP0 for SDI2
     #endif
     
     __builtin_write_OSCCONL(OSCCON | 0x40); // lock   PPS
     
     #endif
     
     // initialize the Flash Memory driver
     FlashInit(&SPI_Init_Data);
     
     // initialize the timer that manages the tick counter
     TickInit();
     
     // initialize the components for Resistive Touch Screen
     TouchInit(NVMWrite, NVMRead, NVMSectorErase, TOUCH_INIT_VALUES);
     
     HardwareButtonInit(); // Initialize the hardware buttons
     
   }
   
   
